import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import ProductCard from '../components/ProductCard';
import FeaturedProducts from '../components/FeaturedProducts';


export default function Home() {

	const data = {
		title: "BLACKPINK | BORN PINK",
		content: "FREE LIGHT STICK KEY RING WITH ALL PURCHASES OVER ₱2,500 WHILE SUPPLIES LAST. FREE SHIPPING ON ALL ORDERS OVER ₱1,000.",
		destination: "/products",
		label: "Shop now!"
	}

	return (
		<div className="home-background">
			<Banner data={data}/>
			<FeaturedProducts />
			<Highlights/>
			{/*<ProductCard />*/}
		</div>
	)
};