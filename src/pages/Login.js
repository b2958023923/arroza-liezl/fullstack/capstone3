import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login() {


	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [ isActive, setIsActive] = useState(false);

	function authenticateUser(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data.access) {

				
				localStorage.setItem("token", data.access);

				
				retrieveUserDetails(data.access);

				
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text:"Welcome to Liezl Shop!",
				});
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text:"Check your login details and try again."
				});
			}
		})

		setEmail('');
		setPassword('');
	};

	const retrieveUserDetails = (token) =>{

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		})
	}

	useEffect(() => {
		if(email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(

		(user.id !== null) ?
			<Navigate to="/products" />
			:
				<Form onSubmit={e => authenticateUser(e)} >
				  <h1 className="my-3 text-center">Login</h1>

				  <Form.Group className="mb-3" controlId="Email address">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter your email"  
				    	required
				    	value={email}
				    	onChange={e => {setEmail(e.target.value)}}
				    />
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="Password1">
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Enter password" 
				    	required 
				    	value={password}
				    	onChange={e => {setPassword(e.target.value)}}
				    />
				  </Form.Group>

				  {
				  	isActive 
				  		? <Button variant="dark" type="submit" id="submitBtn">Submit</Button>
				  		: <Button variant="dark" type="submit" id="submitBtn" disabled>Submit</Button>
				  }

				</Form>


	)
};