import { useState, useEffect, useContext } from 'react';
import {Row, Col} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Profile(){

    const {user} = useContext(UserContext);

    const [details,setDetails] = useState({})

    // const [orderHistory, setOrderHistory] = useState([]);


    useEffect(() => {
        // Fetch user details
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (typeof data._id !== "undefined") {
                setDetails(data);
            }
        });

        // Fetch user's order history
        // fetch(`${process.env.REACT_APP_API_URL}/orders/orderHistory`, {
        //     headers: {
        //         Authorization: `Bearer ${localStorage.getItem('token')}`
        //     }
        // })
        // .then(res => res.json())
        // .then(data => {
        //     setOrderHistory(data);
        // });
    }, []);


    return (
        (user.id === null) ?
        <Navigate to="/products" />
        :
        <div className="profile-container">
            <Row>
                <Col className="p-5 ">
                    <h1 className="my-5 text-center">Profile</h1>
                    <h2 className="mt-3">{`${details.fullname}`}</h2>
                    <hr />
                    <h4>Contacts</h4>
                    <ul>
                        <li>Email: {details.email}</li>
                        <li>Phone number: {details.phoneNumber}</li>
                    </ul>
                    
                    {/* Display Order History */}
                    {/*<h2 className="my-5 text-center">Order History</h2>
                    <ul>
                        {orderHistory.map(order => (
                            <li key={order.id}>
                                Order ID: {order.id} - Total: ${order.total}
                            </li>
                        ))}
                    </ul>*/}
                </Col>
            </Row>
        </div>
    );

}
