import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register() {

	const {user} = useContext(UserContext);

	const [fullname, setFullname] = useState("");
	const [username, setUsername] = useState("");
	const [email, setEmail] = useState("");
	const [phoneNumber, setPhoneNumber] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");


	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	console.log(fullname);
	console.log(username);
	console.log(email);
	console.log(phoneNumber);
	console.log(password);
	console.log(confirmPassword);

	function registerUser(e) {
	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
	        method: "POST",
	        headers: {
	            "Content-Type": "application/json"
	        },
	        body: JSON.stringify({
	            email: email
	        })
	    })
	    .then(res => res.json())
	    .then(data => {

	        if(data) {
	            Swal.fire({
	            	title: 'Error!',
	            	icon: 'error',
	            	text: 'Duplicate email found!'
	            })
	        } else {

	            fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
	                method: "POST",
	                headers: {
	                    "Content-Type": "application/json"
	                },
	                body: JSON.stringify({
	                    fullname: fullname,
	                    username: username,
	                    email: email,
	                    phoneNumber: phoneNumber,
	                    password: password
	                })
	            })
	            .then(res => res.json())
	            .then(data => {
	                console.log(data);

	                if(data) {

	                    setFullname("");
	                    setUsername("");
	                    setEmail("");
	                    setPhoneNumber("");
	                    setPassword("");
	                    setConfirmPassword("");

	                    Swal.fire({
	                    	title: 'Success!',
	                    	icon: 'success',
	                    	text: 'You successfully registered'
	                    })

	                    navigate('/login');

	                } else {

	                    Swal.fire({
	                    	title: 'Error!',
	                    	icon: 'error',
	                    	text: 'Please try again'
	                    })
	                }

	            })
	        }


	    })

	  };
	useEffect(() => {

		if((fullname !== "" && username !== "" && email !== "" && phoneNumber !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (phoneNumber.length === 11)) {
				setIsActive(true)
		} else {

				setIsActive(false)
		}

	}, [fullname, username, email, phoneNumber, password, confirmPassword]);


	return (
		(user.id !== null) ?
		    <Navigate to="/products" />
		:

		<Form onSubmit={e => registerUser(e)}>
			<h1 className="my-5 text-center" >Register</h1>

	      <Form.Group className="mb-3" controlId="Fullname">
	        <Form.Label>Fullname</Form.Label>
	        <Form.Control 
	        	type="text" 
	        	placeholder="Enter fullname" 
	        	required
	        	value={fullname}
	        	onChange={e => {setFullname(e.target.value)}}
	        />
	      </Form.Group>	 

	      <Form.Group className="mb-3" controlId="Username">
	        <Form.Label>Username</Form.Label>
	        <Form.Control 
	        	type="text" 
	        	placeholder="Enter username" 
	        	required
	        	value={username}
	        	onChange={e => {setUsername(e.target.value)}}

	        />
	      </Form.Group>	

	      <Form.Group className="mb-3" controlId="Email address">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        	type="email" 
	        	placeholder="name@example.com" 
	        	required 
	        	value={email}
	        	onChange={e => {setEmail(e.target.value)}}
	        />
	      </Form.Group>	

	      <Form.Group className="mb-3" controlId="Phone number">
	        <Form.Label>Phone number</Form.Label>
	        <Form.Control 
	        	type="text" 
	        	placeholder="Enter 11-digit number" 
	        	required 
	        	value={phoneNumber}
	        	onChange={e => {setPhoneNumber(e.target.value)}}
	        />
	      </Form.Group>	 

	      <Form.Group className="mb-3" controlId="Password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	placeholder="Enter password" 
	        	required 
	        	value={password}
	        	onChange={e => {setPassword(e.target.value)}}
	        />
	      </Form.Group>	 

	      <Form.Group className="mb-3" controlId="Password2">
	        <Form.Label>Confirm Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	placeholder="Confirm password" 
	        	required 
	        	value={confirmPassword}
	        	onChange={e => {setConfirmPassword(e.target.value)}}
	        />
	      </Form.Group>	 

	      {
	      	isActive
	      		? <Button variant="outline-dark" type="submit" id="submitBtn">Submit</Button>
	      		: <Button variant="dark" type="submit" id="submitBtn" disabled>Submit</Button>
	      }

	    </Form>

	)
};