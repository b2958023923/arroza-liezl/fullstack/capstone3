import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function ProductView() {


	const {productId} = useParams();

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [quantity, setQuantity] = useState(1);
	const [price, setPrice] = useState(0);
	const [totalAmount, setTotalAmount] = useState(0);

	useEffect(()=>{
		// console.log(productId)

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])


	const handleQuantityIncrease = () => {
		    setQuantity((prevQuantity) => prevQuantity + 1);
		  };

  	const handleQuantityDecrease = () => {
    setQuantity((prevQuantity) => Math.max(prevQuantity - 1, 1));
  	};

	  useEffect(() => {
	    // Fetch product details from the server using the productId
	    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
	      .then((res) => res.json())
	      .then((data) => {
	        console.log(data)
	        setName(data.name);
	        setDescription(data.description);
	        setPrice(data.price);
	      })
	      .catch((error) => {
	        console.error('Error fetching product details:', error);
	      });
	  }, [productId]);

	  useEffect(() => {
	    setTotalAmount(price * quantity);
	  }, [price, quantity]);

	  const checkout = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data){
				Swal.fire({
					title: "Order placed successfully",
					icon: "success",
					text: "Your order has been placed successfully."
				})


				navigate("/products")
			}
			else{
				Swal.fire({
					title: "Error placing the order",
					icon: "error",
					text: "Admins are not allowed to create order."
				})
			}

		});
	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>				
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>	
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>₱ {price}</Card.Text>
							<Card.Subtitle>Quantity:</Card.Subtitle>
							<div className="p-3">
								<Button variant="outline-danger" onClick={handleQuantityDecrease} disabled={quantity <= 1}>-</Button>
								<span>	{quantity}	</span>
								<Button variant="outline-danger" onClick={handleQuantityIncrease}>+</Button>
							</div>
							<Card.Subtitle>Total Amount: ₱ {totalAmount}</Card.Subtitle>
							</Card.Body>
							<Card.Footer className="text-center">
							{user.id !== null 
							?
								<Button variant="dark" onClick={() => checkout(productId)}>Checkout</Button>
							:
								<Button as={Link} to="/login" variant="danger">Log in to checkout</Button>
							}
						
							</Card.Footer>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}