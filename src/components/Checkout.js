import React, { useState } from 'react';

const Checkout = ({ token }) => {
  const [checkoutStatus, setCheckoutStatus] = useState('');

  const handleCheckout = () => {
    // Perform the checkout using the token
    fetch('/api/checkout', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => setCheckoutStatus(data.status))
      .catch((error) => console.error('Error during checkout:', error));
  };

  return (
    <div>
      <h2>Checkout</h2>
      {checkoutStatus ? (
        <p>{checkoutStatus}</p>
      ) : (
        <button onClick={handleCheckout} className="btn btn-primary">
          Checkout
        </button>
      )}
    </div>
  );
};

export default Checkout;
