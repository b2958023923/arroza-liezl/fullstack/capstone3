import { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

    console.log(productProp);

    const { _id, name, description, price } = productProp;

    console.log(name);

    return (
        
        <Row className="m-3">
            <Col className="container text-center" xs={12} md={6}>
                <Card className="cardHighlight p-3" border="dark">
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>₱ {price}</Card.Text>
                        <Button as={Link} to={`/products/${_id}`} variant="dark">Checkout</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
};