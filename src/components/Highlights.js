import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {

	return (

		<Row className="my-3">
			<Col xs={12} md={6} >
				<Card className="cardHighlight p-3 my-5" >
			     <Card.Body>
			       <Card.Title className="text-center pb-3">Shop Apparel</Card.Title>
			       <Card.Text>
			         Step into the world of BLACKPINK fashion with our exclusive apparel collection! Discover a trendy array of stylish clothing inspired by the iconic Kpop girl group. From chic streetwear to glamorous stage outfits, each piece is meticulously designed to reflect BLACKPINK's unique style and charisma. Show your love and support for Jisoo, Jennie, Rosé, and Lisa as you rock these fashionable items, embodying the confidence and glamour that BLACKPINK exudes!
			       </Card.Text>
			     </Card.Body>
			   </Card>
			</Col>

			

			<Col xs={12} md={6} >
				<Card className="cardHighlight p-3 my-5" >
			     <Card.Body>
			       <Card.Title className="text-center pb-3">Shop Accessories</Card.Title>
			       <Card.Text>
			          Elevate your style with the sensational accessories line, dedicated to BLACKPINK's one-of-a-kind charm. Unveil a captivating selection of finely crafted jewelry, stylish bags, and eye-catching accessories inspired by the individual members' distinct personalities. Whether you want to shine like Rosé, exude elegance like Jisoo, radiate confidence like Jennie, or embrace a fierce spirit like Lisa, our BLACKPINK-inspired accessories will help you channel your inner Kpop star and showcase your adoration for the sensational girl group.
			       </Card.Text>
			     </Card.Body>
			   </Card>
			</Col>
		</Row>
	)
};