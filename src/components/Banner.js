import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner({data}) {

	console.log(data)

	const {title, content, destination, label} = data;

	return (

		<Row>
			<Col className="p-5 text-center">
				<h2 className="p-3 text-center">{title}</h2>
				<h6 className="p-3 text-center">{content}</h6>
				<Link className="btn btn-dark" to={destination} >{label}</Link>
			</Col>
		</Row>



	)
}